# Online Appendix to Knowledge Base Extraction for Learning Agents

This is the online appendix (Appendix B) to the PhD thesis on knowledge base extraction for learning agents in the context of games by Daan Apeldoorn.


## B.1&nbsp;&nbsp;Comparison of Pure Q-Learning vs Q-Learning with HKB Heuristics Extracted During the Learning&nbsp;Process <br /> (accompanies Section 5.1.2 of the thesis)

The first video (Section B.1.1) shows an agent that uses a common *Q-learning* approach (Watkins, Sutton & Barto) to learn to win the game *Camel Race* from the framework of the *general video game artificial intelligence* (GVGAI) competition (Perez-Liebana et al.). In this game, the agent controls the yellow camel in the middle and has to reach one of the goals on the right. The distance to the fastest camel (the pink camel) is provided to the agent as reward.

The second video (Section B.1.2) shows an agent that extracts HKBs and exploits them as heuristics during the learning process of the same Q-learning approach, resulting in an acceleration of the learning process.

### B.1.1&nbsp;&nbsp;Agent Using a Common Q-Learning Approach

Even if the agent makes some progress, it is not able to achieve one of the goals on the right within the provided time.

<a href="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/PureQLearning.mp4" target="_blank"><img src="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/PureQLearning.png" width="75%" /></a>

### B.1.2&nbsp;&nbsp;Agent Extracting and Exploiting HKBs as Heuristics During the Q-Learning Process

The agent presented here is based on the agent model shown in Figure 5.5 of the thesis and shows a different learning behavior: By extracting and exploiting HKB heuristics during the learning process, the agent quickly reaches one of the goals after a couple of iterations only.

<a href="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/QLearningWithExtractedHKBHeuristics.mp4" target="_blank"><img src="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/QLearningWithExtractedHKBHeuristics.png" width="75%" /></a>


## B.2&nbsp;&nbsp;Demonstration of HKB Forward Model&nbsp;Learning <br /> (accompanies Section 5.2 of the thesis)

This video of the game *Butterflies* from the framework of the *general video game artificial intelligence* (GVGAI) competition (Perez-Liebana et al.) demonstrates the forward model learning approach based on HKBs (Dockhorn & Apeldoorn). <br /> In this game, an agent controls a fairy that has to collect butterflies. The agent does not know the game to be played in advance and starts without a forward model of the game. 

The agent is based on the agent model from Section 5.2.3 of the thesis (see Figure 5.9), which combines learning and revision approaches using HKBs. 

At the beginning, the agent exploits the environment with random actions. Toward the end of the video, it can be seen that the agent learned the forward model (how movement and scoring works), which allows for applying a *monte carlo tree search* (MCTS) approach (see, e.&#8239;g., Browne et al. 2012).

<a href="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/HKBForwardModelLearning.mp4" target="_blank"><img src="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/HKBForwardModelLearning.png" width="75%" /></a>


## B.3&nbsp;&nbsp;Exhibit on the Measurement of Subjective Strategic Depth at Deutsches Museum Bonn <br /> (accompanies Section 4.2 of the thesis)

This figure shows an exhibit of the AI exhibition at the <a href="https://www.deutsches-museum.de/bonn" target="_blank">Deutsches Musuem Bonn</a> (German Museum in Bonn) concerning the HKB-based measurement of *subjectively experienced strategic depth* when playing a game (Apeldoorn & Volz 2017). The exhibit's software was originally created by the author for the <a href="https://www.z-quadrat-mainz.de/" target="_blank">Z Quadrat GmbH</a> (an educational company focusing on mathematics and computer science didactics). 

The exhibit consists of a horse racing game (cf. Figure 3.2 of the thesis) with different levels of increasing difficulty. In each level, the player has to reach a trophy within a limited time, by navigating a horse around rocks and jumping over hurdles. After successfully finishing a level, an HKB is extracted from the player's recent playtrace and the exhibit shows the knowledge in the form of rules with exceptions. Moreover, an estimate of the strategic depth that was experienced by the player is calculated from the extracted HKB (see Definition 4.1 of the thesis) and presented to the player.

Using the exhibit, the player can evaluate the strategic depth estimates against its own feelings when playing the different levels of increasing difficulty. By this means, the player can learn about the representation of knowledge in the form of rules with exceptions and how this kind of knowledge representation relates to the subjectively experienced strategic depth when playing a game.

<img src="https://gitlab.com/kb-extraction-for-learning-agents/online-appendix/-/raw/main/DaanWithExhibitAtDeutschesMuseumBonn.jpg" width="75%" />
